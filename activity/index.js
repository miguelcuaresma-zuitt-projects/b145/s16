console.log("Hello World");

let num1 = 8;
let num2 = 2;

let addition = num1 + num2;
console.log(addition)
let subtraction = num1 - num2;
console.log(subtraction)
let multiplication = num1 * num2;
console.log(multiplication)
let division = num1 / num2;
console.log(division)

let comparison1 = addition > subtraction;
console.log("Is the sum greater that the sum? "+ comparison1);

let comparison2 = multiplication && division >= 0;
console.log("Are product and quotient positive numbers? " + comparison2);

let comparison3 = (addition || subtraction || multiplication || division) <= 0;
console.log("Is any of them a negative number? " + comparison3);

let comparison4 = (addition || subtraction || multiplication || division ) ===  0;
console.log("Is any of them equal to zero? " + comparison4);