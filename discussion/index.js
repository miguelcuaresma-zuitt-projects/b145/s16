console.log("hello world");

// SECTION 1 - Assignment Operators

//1. Basic Assignment Operator (=)
// this allows us to add the value or the right operabd to a variable and assigns the result to the variable.

let assignmentNumber = 5;
let message = "This is the message";

//2. Addition assignment operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2
console.log("Result of the operation: " + assignmentNumber);

// SECTION 1.1 - Arrithmetic Operators 
// ( + - * / )
// 3. Subtraction/Multiplication/Division Assignment Operator
assignmentNumber -= 3;
console.log("Result of the operation is: " + assignmentNumber);

// SECTION - Arithmethic Operators

let x = 15;
let y = 10;

// addition +
let sum = x + y;
console.log(sum);

// subtraction -
let difference = x - y;
console.log(difference);

// multiplication *
let product = x * y;
console.log(product);

// division /
let quotient = x / y; console.log(quotient);

// remainder between the values (modulus % )
let remainder = x % y;
console.log(remainder);

// SUBSECTION - Multiple operators and parentheses
// when multiple operators are applied in a single statement, it follows the PEMDAS rule. (parenthesis, exponent, multiplication, division, addition, and subtraction)
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// SECTION - Increment and Decrement
let z = 1;

// Pre and Post
// pre-increment (syntax: ++variable)
let preIncrement = ++z; // 1 + z
console.log(preIncrement); // 2


// post-increment(syntax: variable++)
let postIncrement = z++; // z = 2, z + 1
/*the value of z is returned and stored inside the variable called "postIncrement".
the value of z is at 2 before it was incremented.*/
console.log(postIncrement); // 2
// z + 1
console.log(z); // 3
// 1 + z 
// z + 1

// Decrement --
let preDecrement = --z; //z = 3
console.log(preDecrement); //2

let postDecrement = z--; // z = 2, z - 1
/*the value of z is returned and stored inside the variable called "postdecrement". 
the value of z is at 2 before it was decremented.*/
console.log(postDecrement); // 2
// z - 1
console.log(z); // 1

// SECTION - TYPE COERCION
let numberA = 6;
let numberB = '6';
// lets check the data types of the values above
// typeof expression - will allows us to identify the data type of a certain value/component
console.log(typeof numberA);
console.log(typeof numberB);
let coercion = numberA + numberB;
console.log(coercion);

// Adding number and boolean
let expressionC = 10 + true; // 10 + 1
console.log(expressionC);

let expressionD = 10 + false; // 10 + 0
console.log(expressionD);

let expressionF = true + false; // 1 + 0
console.log(expressionF);

let expressionG = 8 + null; // 8 + 0
console.log(expressionG);
// null was converted into a primitive data type.

// Coversion rules
 /*
	1. If atleast one operand is an object, it will be coverted into a primitive value/data type.

	2. After conversion, if atleast 1 operand is a string data type, 
	   the 2nd operand will be coverted into  another string to perform concatenation.

	3. In other cases where both operands are converted to numbers then arithmetic operation is executed.
 */

let expressionH = "Batch145" + null; // "Batch145" + "null"
console.log(expressionH);

let expressionI = 9 + undefined; // 9 + NaN = NaN
console.log(expressionI);

// SECTION 2 - Comparison Operators
let name = "Juan";

// 2.1 Equality operators ==

console.log("Juan" == name); // true

// 2.2 Inequality operators !=
console.log(1 != 1) // false

// 2.3 Strict equality operators ===
// also compares if the data types are the same

console.log(1 === 1); //true
console.log(1 === "1"); //false

// 2.4 Strict inequality operators !===

console.log(1 !== 1); //false
console.log(1 !== "1"); //true

/*Developer's tip: upon creating conditions or statement it is strongly recommended to use "strict" equality 
operators over "loose" equality operators because it will be easier for us to predetermine outcomes 
and results in any given scenario.*/

// SECTION Relational operators
let priceA = 1800;
let priceB = 1450;

// less than <
console.log(priceA < priceB);//false

// greater than >
console.log(priceA > priceB);//true

/*
	Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. 
	it is writing convention for developers to add a prefix of "is" or "are" together with the variable 
	name to form a variable similar on how to answer a simple yes or no question.
*/

isLegalAge = true;
isRegistered = false;
// AND operator &&

let isAllowedToVote = isRegistered && isLegalAge;
console.log("Is the person allowed to vote? " + isAllowedToVote);

// OR operator ||
let isAllowedForVaccination = isLegalAge || isRegistered;
console.log("Did the person pass? " + isAllowedForVaccination);

// NOT operator !
let isTaken = true;
let isTalented = false;

console.log(!isTaken)//false
console.log(!isTalented)//true